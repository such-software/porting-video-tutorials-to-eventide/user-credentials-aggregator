require_relative '../automated_init'

context "Handle Identity Events" do
  context "Registered" do
    registered = Identity::Client::Controls::Events::Registered.example

    user_id = registered.user_id or fail
    email = registered.email or fail
    password_hash = registered.password_hash or fail

    handler = Handlers::Identity::Events.new

    handler.(registered)

    create_user_credential = handler.create_user_credential

    test "We called the query with the right parameters" do
      assert(create_user_credential.called?(user_id, email, password_hash))
    end
  end
end
