require_relative "../../../automated_init"

context "Queries" do
  context "CreateUserCredential" do
    context "Substitute" do
      context "Called" do
        create_user_credential = Queries::CreateUserCredential::Substitute.build

        user_id = Identity::Client::Controls::ID.example or fail
        email = Identity::Client::Controls::Email.example or fail
        password_hash = Identity::Client::Controls::PasswordHash.example

        refute(create_user_credential.called?(user_id, email, password_hash))

        create_user_credential.(user_id, email, password_hash)

        test "It was called" do
          assert(create_user_credential.called?(user_id, email, password_hash))
        end
      end
    end
  end
end

