require_relative "../test_init"

user_id = Identifier::UUID::Random.get
email = "example@example.com"
password_hash = "hashiddyhash"

create_user_credential = Queries::CreateUserCredential.build

create_user_credential.(user_id, email, password_hash)
