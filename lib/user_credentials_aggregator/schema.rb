module UserCredentialsAggregator
  module Schema
    EXCLUDE = [ '.', '..' ]

    def self.call
      sql_files_path = File.join(
        __dir__,
        "schema"
      )

      sql_files = Dir.entries(sql_files_path).filter { |f| !EXCLUDE.include?(f) }.sort

      settings = ViewDataDatabaseSettings.instance
      session = Session.build(settings:)

      session.execute("SET client_min_messages = warning")

      sql_files.each do |filename|
        file_path = File.join(sql_files_path, filename)

        File.open(file_path, "r") do |file|
          contents = file.read

          session.execute(contents)
        end
      end

      session.close
    end
  end
end
