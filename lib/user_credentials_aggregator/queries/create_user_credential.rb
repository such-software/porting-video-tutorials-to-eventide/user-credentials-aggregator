module UserCredentialsAggregator
  module Queries
    class CreateUserCredential
      include Dependency
      include Log::Dependency

      dependency :session, Session

      def self.build
        new.tap do |instance|
          settings = ViewDataDatabaseSettings.instance
          Session.configure(instance, settings:)
        end
      end

      def self.configure(receiver)
        instance = build

        receiver.create_user_credential = instance
      end

      def call(user_id, email, password_hash)
        logger.info(tags: :create_user_credential) { "CreateUserCredentail called: (user_id: #{user_id}), (email: #{email}), (password_hash: #{password_hash})" }

        params = [user_id, email, password_hash]

        session.execute(self.class.statement, params)
      end

      def self.statement
        <<-SQL
          INSERT INTO
            user_credentials (id, email, password_hash)
          VALUES
            ($1::uuid, $2::varchar, $3::varchar)
          ON CONFLICT DO NOTHING
        SQL
      end

      module Substitute
        def self.build
          CreateUserCredential.new
        end

        class CreateUserCredential
          def call(user_id, email, password_hash)
            calls.push({ user_id:, email:, password_hash: })
          end

          def calls
            @calls ||= []
          end

          def called?(user_id, email, password_hash)
            !!calls.index do |c|
              c[:user_id] == user_id  &&
                c[:email] == email  &&
                c[:password_hash] == password_hash
            end
          end
        end
      end
    end
  end
end
