module UserCredentialsAggregator
  module Start
    def self.call
      Schema.()

      Consumers::Identity::Events.start("identity")
    end
  end
end
