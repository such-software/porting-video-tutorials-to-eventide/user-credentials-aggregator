module UserCredentialsAggregator
  module Consumers
    module Identity
      class Events
        include Consumer::Postgres

        handler Handlers::Identity::Events
      end
    end
  end
end
