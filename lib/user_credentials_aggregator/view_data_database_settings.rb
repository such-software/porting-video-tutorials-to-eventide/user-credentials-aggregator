module UserCredentialsAggregator
  class ViewDataDatabaseSettings < ::Settings
    def self.instance
      @instance ||= build
    end

    def self.data_source
      Defaults.data_source
    end

    def self.names
      [
        :dbname,
        :host,
        :hostaddr,
        :port,
        :user,
        :password,
        :connect_timeout,
        :options,
        :sslmode,
        :krbsrvname,
        :gsslib,
        :service,
        :keepalives,
        :keepalives_idle,
        :keepalives_interval,
        :keepalives_count,
        :tcp_user_timeout
      ]
    end

    class Defaults
      def self.data_source
        ENV['VIEW_DATA_DATABASE_SETTINGS_PATH'] || 'settings/view_data_database.json'
      end
    end
  end
end

