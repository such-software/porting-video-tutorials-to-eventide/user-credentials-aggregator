module UserCredentialsAggregator
  module Handlers
    module Identity
      class Events
        include Messaging::Handle
        include Messaging::StreamName
        include Log::Dependency

        include ::Identity::Client::Messages::Events

        dependency :create_user_credential, Queries::CreateUserCredential

        def configure
          Queries::CreateUserCredential.configure(self)
        end

        handle Registered do |registered|
          create_user_credential.(
            registered.identity_id,
            registered.email,
            registered.password_hash
          )
        end
      end
    end
  end
end
