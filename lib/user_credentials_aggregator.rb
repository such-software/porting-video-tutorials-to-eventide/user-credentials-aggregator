require "eventide/postgres"
require "identity/client"

require "user_credentials_aggregator/load"

require "user_credentials_aggregator/queries/create_user_credential"

require "user_credentials_aggregator/handlers/identity/events"

require "user_credentials_aggregator/consumers/identity/events"

require "user_credentials_aggregator/start"
