# -*- encoding: utf-8 -*-
Gem::Specification.new do |s|
  s.name = 'user_credentials_schema-client'
  s.version = '0.0.0'
  s.summary = 'User Credentials Schema Client'
  s.description = ' '

  s.authors = ['Ethan Garofolo']
  s.email = 'ethan@suchsoftware.com'
  s.homepage = 'https://gitlab.com/such-software/porting-video-tutorials-to-eventide/user-credentials-aggregator'
  s.licenses = ['MIT']

  s.require_paths = ['lib']
  s.files = Dir.glob('{lib}/**/*')
  s.platform = Gem::Platform::RUBY
  s.required_ruby_version = '>= 2.4'

  s.require_paths = ['lib']

  files = Dir["lib/user_credentials_schema/**/*.rb"]

  files << "lib/user_credentials_aggregator/load.rb"

  File.read("lib/user_credentials_aggregator/load.rb").each_line.map do |line|
    next if line == "\n"

    _, filename = line.split(/[[:blank:]]+/, 2)

    filename.gsub!(/['"]/, '')
    filename.strip!

    filename = File.join('lib', "#{filename}.rb")

    if File.exist?(filename)
      files << filename
    end
  end

  s.files = files

  s.platform = Gem::Platform::RUBY
  s.required_ruby_version = '>= 2.4'

  s.add_runtime_dependency 'evt-log'
  s.add_runtime_dependency 'evt-settings'

  s.add_runtime_dependency 'pg'
end
